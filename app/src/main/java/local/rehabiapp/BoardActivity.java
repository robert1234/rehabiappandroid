package local.rehabiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import local.rehabiapp.utils.SaveSharedPreference;

public class BoardActivity extends AppCompatActivity {

    private Button steps;
    private Button config;
    private Button logout;
    private TextView infoboxUser;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);

        steps = findViewById(R.id.steps);
        config = findViewById(R.id.config);
        logout = findViewById(R.id.logout);
        infoboxUser = findViewById(R.id.infoboxUser);

        infoboxUser.setText("Zalogowano jako: "+SaveSharedPreference.getLastUsername(getApplicationContext()));

        final Context self = this;

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutAction();
            }
        });

        steps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startViewActivity(self,StepsActivity.class);
            }
        });
    }

    protected void logoutAction() {
        Log.d("Active",SaveSharedPreference.getLastPassword(getApplicationContext()));
        if((Boolean)SaveSharedPreference.getStatus(getApplicationContext())) {
            SaveSharedPreference.setCredentials(getApplicationContext(),"","");
            SaveSharedPreference.setStatus(getApplicationContext(), false);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            this.finish();
        }
    }

    protected void startViewActivity(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        startActivity(intent);
    }
}
