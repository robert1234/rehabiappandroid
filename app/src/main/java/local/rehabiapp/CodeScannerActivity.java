package local.rehabiapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import local.rehabiapp.net.HTTPManager;

public class CodeScannerActivity extends AppCompatActivity {

    private Integer stepId;
    private TextView codeScannerInformationArea;
    private TextView codeScannerConfirmationArea;
    private SurfaceView codeScannerCamera;

    CameraSource cameraSource;
    BarcodeDetector barcodeDetector;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_scanner);
        this.stepId = Objects.requireNonNull(getIntent().getExtras()).getInt("stepId");
        this.codeScannerInformationArea = findViewById(R.id.code_scanner_information_area);
        this.codeScannerConfirmationArea = findViewById(R.id.code_scanner_confirmation_area);
        this.codeScannerCamera = findViewById(R.id.code_scanner_camera);

        final int[] size = new int[2];

        size[0] = 1024;
        size[1] = 1024;

        this.codeScannerInformationArea.setText(this.codeScannerInformationArea.getText()+"\nNumer Etapu: "+stepId);

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE).build();

        cameraSource = new CameraSource.Builder(this,barcodeDetector)
                .setRequestedPreviewSize(size[0], size[1]).setAutoFocusEnabled(true).build();

        codeScannerCamera.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if(ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                    return;
                }
                try {
                    cameraSource.start(holder);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {}

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            private Boolean lock = false;

            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrCodes = detections.getDetectedItems();

                if ( qrCodes.size() != 0 ) {
                    if(!lock){
                        lock = true;
                        codeScannerConfirmationArea.post(new Runnable() {
                            @Override
                            public void run() {
                                Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                assert vibrator != null;
                                vibrator.vibrate(100);
                                codeScannerConfirmationArea.setText("Przetwarzanie...");
                                sendCodeToValidate(stepId,qrCodes.valueAt(0).displayValue);
                            }
                        });
                    }
                }else{
                    lock = false;
                }
            }
        });
    }

    protected void sendCodeToValidate(int stepId, final String validationCode) {
        String validationUri = "etap/modify/qrcode";
        RequestParams requestParams = new RequestParams();
        requestParams.add("etap_id", String.valueOf(stepId));
        requestParams.add("validation_code",validationCode);
        final CodeScannerActivity self = this;
        HTTPManager.post(getApplicationContext(), validationUri, requestParams, new JsonHttpResponseHandler(){
            @SuppressLint("SetTextI18n")
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Log.d("Message", (String) response.get("message"));
                    if(statusCode == 200 && ((String) response.get("message")).equals("success")) {
                        codeScannerConfirmationArea.setText("Kod: "+validationCode + "\nPotwierdzono Zabieg");
                        Intent intent = new Intent(self, StepsActivity.class);
                        startActivity(intent);
                        self.finish();
                    }
                    if(statusCode == 200 && ((String) response.get("message")).equals("bad_code_or_user")) {
                        codeScannerConfirmationArea.setText("Kod: "+validationCode + "\nNieprawidłowy Kod");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @SuppressLint("SetTextI18n")
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("Failed: ", ""+statusCode);
                Log.d("Error : ", "" + throwable);
                codeScannerConfirmationArea.setText("Kod: "+validationCode + "\nWystąpił Bład");
            }
        });
    }
}
