package local.rehabiapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import local.rehabiapp.net.HTTPManager;
import local.rehabiapp.utils.SaveSharedPreference;

public class LoginActivity extends AppCompatActivity {
    LinearLayout form;
    EditText username;
    EditText password;
    Button submit;
    TextView info;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.form = findViewById(R.id.form);
        this.username = findViewById(R.id.username);
        this.password = findViewById(R.id.password);
        this.submit = findViewById(R.id.submit);
        this.info = findViewById(R.id.infobox);

//        BottomNavigationView navView = findViewById(R.id.nav_view);
//        // Passing each menu ID as a set of Ids because each
//        // menu should be considered as top level destinations.
//        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
//                .build();
//        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
//        NavigationUI.setupWithNavController(navView, navController);

        // Ładowanie aktywności "lista zabiegów użytkownika" jeśli zalogowano
        Log.v("Active","" + (Boolean)SaveSharedPreference.getStatus(getApplicationContext()));
        if((Boolean)SaveSharedPreference.getStatus(getApplicationContext())) {
            Intent intent = new Intent(this, BoardActivity.class);
            startActivity(intent);
            this.finish();
        } else {
            this.form.setVisibility(View.VISIBLE);
        }

        // Akcja przycisku
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginAction(username.getText().toString(), password.getText().toString());
            }
        });

    }

    protected void loginAction(String username, String password) {
        String checkUri = "check";
        final LoginActivity self = this;

        SaveSharedPreference.setCredentials(getApplicationContext(), username, password);
        HTTPManager.get(getApplicationContext(), checkUri, null, new JsonHttpResponseHandler(){
            @SuppressLint("SetTextI18n")
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Log.d("Message", (String) response.get("message"));
                    if(statusCode == 200 && ((String) response.get("message")).equals("hello")) {
                        SaveSharedPreference.setStatus(getApplicationContext(), true);
                        info.setText("Zalogowano");
                        Intent intent = new Intent(self, BoardActivity.class);
                        startActivity(intent);
                        self.finish();
                    } else {
                        info.setText("Błędne dane logowania");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @SuppressLint("SetTextI18n")
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("Failed: ", ""+statusCode);
                Log.d("Error : ", "" + throwable);
                info.setText("Błędne dane logowania");
            }
        });
    }

}
