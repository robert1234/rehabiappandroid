package local.rehabiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import local.rehabiapp.adapter.StepsArrayAdapter;
import local.rehabiapp.net.HTTPManager;

public class StepsActivity extends AppCompatActivity {

    ListView listView;
    List<JSONObject> jsonList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steps);
        getListOfSteps();
        listView = (ListView)findViewById(R.id.listview);
    }

    public void getListOfSteps() {
        jsonList = new ArrayList<>();
        String uri = "etap/list";
        final Context self = this;
        HTTPManager.get(getApplicationContext(), uri, null, new JsonHttpResponseHandler(){
            @SuppressLint("SetTextI18n")
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Log.d("Message", (String) response.get("message"));
                    if(statusCode == 200) {
                        try {
                            JSONArray jsonArray = (JSONArray) response.get("data");
                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                jsonList.add(jsonObject);
                            }
                            StepsArrayAdapter adapter = new StepsArrayAdapter(self, jsonList);
                            listView.setAdapter(adapter);
                        } catch (Exception ignored){}
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @SuppressLint("SetTextI18n")
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d("Failed: ", ""+statusCode);
                Log.d("Error : ", "" + throwable);
            }
        });
    }
}
