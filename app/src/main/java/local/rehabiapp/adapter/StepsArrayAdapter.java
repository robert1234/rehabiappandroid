package local.rehabiapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import local.rehabiapp.CodeScannerActivity;
import local.rehabiapp.R;

import java.util.List;

public class StepsArrayAdapter extends ArrayAdapter<JSONObject> {
    private final Context context;
    private final List<JSONObject> values;

    public StepsArrayAdapter(Context context, List<JSONObject> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        assert inflater != null;
        @SuppressLint("ViewHolder")
        View rowView = inflater.inflate(R.layout.fragment_list_view_row, parent, false);

        TextView treatment_view = (TextView) rowView.findViewById(R.id.treatment_view);
        TextView price_view = (TextView) rowView.findViewById(R.id.price_view);
        Button confirm_button = (Button) rowView.findViewById(R.id.confirm_button);



        try {
            final JSONObject stepJsonObject = this.values.get(position);
            JSONObject dateJsonObject = (JSONObject) stepJsonObject.get("dataPrzeprowadzenia");
            final Integer stepId = stepJsonObject.getInt("id");
            treatment_view.setText(
                    stepJsonObject.getString("zabieg")
                    + " w sali: " +
                    stepJsonObject.getString("nrSali")
                    + "\n" + dateJsonObject.getString("date").substring(0,16)
            );
            if(stepJsonObject.getString("nazwa_statusu").equals("odbyto")) {
                price_view.setText("V");
            } else {
                price_view.setText("X");
            }

            confirm_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CodeScannerActivity.class);
                    intent.putExtra("stepId", stepId);
                    context.startActivity(intent);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rowView;
    }
}
