package local.rehabiapp.net;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import local.rehabiapp.utils.SaveSharedPreference;

import static com.loopj.android.http.AsyncHttpClient.log;

public class HTTPManager {
    private static final String BASE_URL = "http://192.168.1.37:8080/api/";
    private static final String USER_AGENT = "RehabiApp/APK";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        configureHeaders(context);
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        configureHeaders(context);
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    private static void configureHeaders(Context context) {
        client.addHeader("X-AUTH-LOGIN", SaveSharedPreference.getLastUsername(context));
        client.addHeader("X-AUTH-PASSWORD", SaveSharedPreference.getLastPassword(context));
        log.d("Login",SaveSharedPreference.getLastUsername(context));
        log.d("Password",SaveSharedPreference.getLastPassword(context));
        client.setUserAgent(USER_AGENT);
    }
}
