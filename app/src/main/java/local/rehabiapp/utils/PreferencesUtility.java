package local.rehabiapp.utils;

public class PreferencesUtility {
    public static final String LAST_USERNAME_PREF = "username";
    public static final String LAST_PASSWORD_PREF = "password";
    public static final String LOGIN_STATUS_PREF = "status";
}
