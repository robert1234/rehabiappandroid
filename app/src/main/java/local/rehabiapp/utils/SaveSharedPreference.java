package local.rehabiapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SaveSharedPreference {

    static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setCredentials(Context context, String username, String password) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(PreferencesUtility.LAST_USERNAME_PREF, username);
        editor.putString(PreferencesUtility.LAST_PASSWORD_PREF, password);
        editor.apply();
    }

    public static void setStatus(Context context, boolean loggedIn) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(PreferencesUtility.LOGIN_STATUS_PREF, loggedIn);
        editor.apply();
    }

    public static String getLastUsername(Context context) {
        return getPreferences(context).getString(PreferencesUtility.LAST_USERNAME_PREF, "");
    }

    public static String getLastPassword(Context context) {
        return getPreferences(context).getString(PreferencesUtility.LAST_PASSWORD_PREF, "");
    }

    public static boolean getStatus(Context context) {
        return getPreferences(context).getBoolean(PreferencesUtility.LOGIN_STATUS_PREF, false);
    }
}